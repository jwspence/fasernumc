# faserIFTMC

Simulation including IFT and FASERnu. To build:

git clone https://gitlab.cern.ch/jwspence/faseriftmc.git
cd faseriftmc
mkdir build
cd build

Setup LCG_95 (LCG_94/96 will not put FASERnu outpuut into row_wise_branch!)
source ../setupLCG.sh LCG_95 x86_64-centos7-gcc7-opt

Build
source ../buildFaser.sh

To run:
cd ../run
bin/faser init_novis.mac FASER.root

This will produce a tracker output file FASER.root and an emulsion output file FASERnu1.root.
To read the tracker output, do
root -l
gSystem->Load("lib64/libFaserRootClassesDict")
TFile *f = new TFile("FASER.root")


## Prerequisites

Access to CERN cvmfs is required; this is automatically available with CERNVM installations.
