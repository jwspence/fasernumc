#include <sstream>

#include "TROOT.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"

#include "RootEventIO.hh"
#include "FaserEvent.hh"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include "FaserTrackerEvent.hh"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4EventManager.hh"
#include "G4Event.hh"
#include "G4AutoLock.hh"

namespace
{
  G4Mutex rootIoMutex = G4MUTEX_INITIALIZER;
  G4Mutex rootIoDataMutex = G4MUTEX_INITIALIZER;
}

G4String RootEventIO::sFileName = "FaserMCEvent_sim.root";
G4String RootEventIO::sTreeName = "faser";
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
G4String RootEventIO::trackerFileName = "FaserMCEvent_digi.root";
G4String RootEventIO::trackerTreeName = "events";
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
RootEventIO* RootEventIO::sInstance = nullptr;

RootEventIO::RootEventIO()
  : fNevents(0)
  , fBranchAdx(nullptr)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  , fTrackerBranchAdx(nullptr)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
{
  // initialize ROOT
  TSystem ts;
  gSystem->Load("libFaserRootClassesDict");

  fFile = new TFile(sFileName, "RECREATE");
  fTree = new TTree(sTreeName, "an event tree");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  fTrackerFile = new TFile(trackerFileName, "recreate");
//  fTrackerFile->cd();
  fTrackerTree = new TTree(trackerTreeName, "space points for tracking");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}

RootEventIO::~RootEventIO()
{
  G4cout << "Deleting RootEventIO instance" << G4endl;
}

RootEventIO* RootEventIO::GetInstance()
{
  G4AutoLock l(&rootIoMutex);

  if (sInstance == nullptr)
  {
      sInstance = new RootEventIO();
  }
  return sInstance;
}

void RootEventIO::SetFileName(G4String name)
{
  G4AutoLock l(&rootIoDataMutex);
  if (name == sFileName) return;
  sFileName = name;

  if (sInstance == nullptr) return;

  G4cout << "Closing previous ROOT file" << G4endl;

  sInstance->Close();
  delete sInstance;
  sInstance = nullptr;

  return;
}

void RootEventIO::Write(FaserEvent* hcont)
{
  G4AutoLock l(&rootIoDataMutex);
  fNevents++;

  std::ostringstream os;
  os << fNevents;
  std::string stevt = "Event_" + os.str();
  G4cout << "Writing " << stevt << " to " << sTreeName << " in "
         << sFileName << "...";

  if (fBranchAdx == nullptr)
  {
    fBranchAdx = hcont;
    fTree->Branch("event", "FaserEvent", &fBranchAdx, 3200, 99);
  }
  else
  {
    fBranchAdx = hcont;
  }
  G4cout << "Set fBranchAdx to " << hcont << "..." << G4endl;
  fTree->Fill();

  G4cout << "done" << G4endl;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void RootEventIO::Write(FaserTrackerEvent* hcont)
{

  G4cout << "Writing space points to " << trackerTreeName << " in "
         << trackerFileName << "..." << G4endl;

  if (fTrackerBranchAdx == nullptr)
  {
    fTrackerBranchAdx = hcont;
    fTrackerTree->Branch("events", "FaserTrackerEvent", &fTrackerBranchAdx, 3200, 99);
  }
  else
  {
    fTrackerBranchAdx = hcont;
  }
  G4cout << "Set fTrackerBranchAdx to " << hcont << "..." << G4endl;
  G4cout << "Write(FaserTrackerEvent) called successfully." << G4endl;
  fTrackerTree->Fill();

  G4cout << "done" << G4endl;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void RootEventIO::Close()
{
  G4AutoLock l(&rootIoDataMutex);
  if (fFile != nullptr)
  {
    fFile->cd();
    if (fTree != nullptr)
    {
      fTree->Write();
    }
    if (fNevents > 0) G4cout << "Wrote a total of " << fNevents << " events to " << sFileName << G4endl;
    fFile->Close();
  }
 //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  if (fTrackerFile != nullptr)
  {
    fTrackerFile->cd();
    if (fTrackerTree != nullptr)
    {
      fTrackerTree->Write();
    }
    if (fNevents > 0) G4cout << "Wrote a total of " << fNevents << " events to " << trackerFileName << G4endl;
    fTrackerFile->Close();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  }
  sInstance = nullptr;
  fNevents = 0;
  delete this;
}
