#pragma once

#include "G4String.hh"

class FaserEvent;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class FaserTrackerEvent;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
class TFile;
class TTree;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class TTrackerFile;
class TTrackerTree;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
class RootEventIO
{
private:
  static RootEventIO* sInstance;
  RootEventIO();

  TFile* fFile;
  TTree* fTree;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  TFile* fTrackerFile;
  TTree* fTrackerTree;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  int fNevents;
  FaserEvent* fBranchAdx;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  FaserTrackerEvent* fTrackerBranchAdx;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  static G4String sFileName;
  static G4String sTreeName;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  static G4String trackerFileName;
  static G4String trackerTreeName;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
public:
  ~RootEventIO();
  static RootEventIO* GetInstance();
  static void SetFileName(G4String name);
  void Write(FaserEvent*);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  void Write(FaserTrackerEvent*);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  void Close();
};

