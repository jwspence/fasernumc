#pragma once
//------------------------------------------------------------------------------
#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4AffineTransform.hh"
#include "FaserDetectorConstruction.hh"
#include "tls.hh"
//------------------------------------------------------------------------------
#include "TVector3.h"
#include <vector>

//------------------------------------------------------------------------------

class FaserTrackerTruthHit : public G4VHit
{
public:
//    inline void* operator new(size_t);
//    inline void  operator delete(void*);


//    const FaserTrackerTruthHit& operator=(const FaserTrackerTruthHit&);
//    G4int operator==(const FaserTrackerTruthHit&);

//    inline void* operator new(size_t);
//    inline void  operator delete(void*);








  





    G4int Plane() const    	       		        { return plane; };
    G4int Module() const          			{ return module; };
    G4int Sensor() const          			{ return sensor; };
    G4int Row() const	             		        { return row; };
    G4int Strip() const    	       		        { return strip; };
    G4double Edep() const           			{ return energy; };
    TVector3 GlobalPos() const 				{ return globalPos; };
//    G4ThreeVector LocalPos() const	  		{ return fLocalPos; };
//    G4AffineTransform Transform() const                 { return fTransform; }

    G4int Track() const				        { return trackID; };
    G4double Energy() const                             { return energy; }
  
//    G4int OriginTrack() const			        { return fOriginTrackID; };


  int       trackID;
  int       plane;
  int       module;
  int       sensor;
  int       row;
  int       strip;
  double    energy;
  TVector3  globalPos;

  FaserTrackerTruthHit();
  FaserTrackerTruthHit(const FaserTrackerTruthHit& right);
//    const FaserSensorHit& operator=(const FaserSensorHit&);
//    G4int operator==(const FaserSensorHit&);
  FaserTrackerTruthHit(
    int trackID_,
    int plane_,
    int module_,
    int sensor_,
    int row_,
    int strip_,
    double energy_,
    TVector3 globalPos_
  ) : trackID {trackID_}
    , plane {plane_}
    , module {module_}
    , sensor {sensor_}
    , row {row_}
    , strip {strip_}
    , energy {energy_}
    , globalPos {globalPos_}
  {
  }

  virtual ~FaserTrackerTruthHit() {
  }

  void print() const;
private:
  static const FaserDetectorConstruction* fDetectorConstruction;
};

typedef G4THitsCollection<FaserTrackerTruthHit> FaserTrackerTruthHitsCollection;

extern G4ThreadLocal G4Allocator<FaserTrackerTruthHit>* FaserTrackerTruthHitAllocator;

//inline void* FaserTrackerTruthHit::operator new(size_t)
//{
//  if (!FaserTrackerTruthHitAllocator)
//    FaserTrackerTruthHitAllocator = new G4Allocator<FaserTrackerTruthHit>;
//  return (void *) FaserTrackerTruthHitAllocator->MallocSingle();
//}

//inline void FaserTrackerTruthHit::operator delete(void* hit)
//{
//  FaserTrackerTruthHitAllocator->FreeSingle((FaserTrackerTruthHit*) hit);
//}

